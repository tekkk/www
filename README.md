Ceci est le code-source du site web de Codeurs en Liberté (www.codeursenliberté.fr).

Le site est généré par [Hugo](http://gohugo.io/) à partir de fichiers HTML et Markdown.
Il contient à la fois les pages statiques du site (liste des sociétaires, page « À propos »)
et des articles de blog.

## Comment développer en local

1. [Installer hugo](https://gohugo.io/getting-started/installing/) ;
2. `make run` pour builder le site et lancer un serveur local.

## Comment écrire un article de blog

Il y a deux façons possibles :

- **Depuis l'interface de GitLab** : cliquez **[sur ce lien](https://gitlab.com/CodeursEnLiberte/www/new/master/content/entreprise?content=%2B%2B%2B%0D%0Atitle%20%3D%20%22%22%0D%0Aauthor%20%3D%20%22Louise%20Michel%22%0D%0Adate%20%3D%20%221970-01-01T00%3A00%3A00%2B01%3A00%22%0D%0A%2B%2B%2B%0D%0A%0D%0A%C3%89crivez%20votre%20article%20ici.%0D%0A%0D%0AVous%20pouvez%20utiliser%20la%20syntaxe%20_Markdown_%20ou%20%3Cem%3EHTML%3C%2Fem%3E.%0D%0A%0D%0AQuand%20vous%20avez%20termin%C3%A9%2C%20commitez%20l%E2%80%99article%20depuis%20l%27%C3%A9diteur%0D%0Aet%20ouvrez%20une%20nouvelle%20merge%20request%20%28vous%20pourrez%20toujours%20faire%20des%20changements%20apr%C3%A8s%29.%0D%0A)** pour écrire et ajouter un article directement depuis GitLab, sans avoir à installer le blog sur votre ordinateur.
- **Sur votre ordinateur** : installez le site en local (voir ci dessous), puis ajoutez un fichier Markdown dans le dossier `content/entreprise/`.

## Comment générer le site en local

Lancer `make build`. Le site est généré dans le dossier `public/`.

## Comment déployer le site

Le site est déployé avec [Netlify](https://netlify.com), un service d'hébergement de sites statiques.

Netlify observe automatiquement le dépôt git, et :

- builde et déploie une pré-version pour chaque merge request,
- builde et déploie en production les nouveaux commits mergés dans master.

L'outil de build utilisé (`hugo`) et la version à utiliser (`HUGO_VERSION`) sont configurés dans le
fichier `netlify.toml` à la racine du site. Les autres réglages sont modifiables dans l'interface
d'administration web de Netlify.
